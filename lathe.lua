------------------------------------------------------------------------
-- Crane wheels X-grooving. G-code automaton and output.
--
-- (c) 2019 Aleksandr Loshkin <1214160@gmail.com>
-- This code is licensed under MIT licence (see LICENCE.TXT for details)
------------------------------------------------------------------------


lathe = {}
		
local nc_state = {
	g_group1 = nil,
	x = nil,
	z = nil,
	speed = nil,
	feed = nil,
	feed_rapid = 10000, -- mm/min
	timer = 0,
}


-------------------
-- Postprocessor --
-------------------


-- Append a word to the existing block
-- using 0x20 (space) as a separator
local function word_append(block, new_word)
	if #block > 0 then
		return block .. " " .. new_word
	else
		return new_word
	end
end


local function word(addr, val)
	return addr .. tostring(val)
end


-- Simplified version to call
local function word_append2(block, addr, val)
	return word_append(block, word(addr, val))
end


local function move(g_group1, x, z, r)
	local res = ""
	
	-- TO-DO: time calculation

	if g_group1 ~= nil and nc_state.g_group1 ~= g_group1 then
		nc_state.g_group1 = g_group1
		res = word_append2(res, "G", g_group1)
	end

	if x ~= nil and nc_state.x ~= x then
		nc_state.x = x
		res = word_append2(res, "X", x)
	end
	
	if z ~= nil and nc_state.z ~= z then
		nc_state.z = z
		res = word_append2(res, "Z", z)
	end
	
	if r ~= nil then
		res = word_append2(res, "R", r)
	end
	
	if #res > 0 then
		print(res)
	end
end


function lathe.rapidXZ(x, z)
	move(0, x, z)
end

function lathe.rapidX(x)
	move(0, x, nil)
end

function lathe.rapidZ(z)
	move(0, nil, z)
end


function lathe.workXZ(x, z)
	move(1, x, z)
end

function lathe.workX(x)	
	move(1, x, nil)
end

function lathe.workZ(z)
	move(1, nil, z)
end


function lathe.radiusCW(x, z, r)
	move(2, x, z, r)
end

function lathe.radiusCCW(x, z, r)
	move(3, x, z, r)
end


function lathe.comment(s)
	print("(" .. s .. ")")
end



------------
-- Timing --
------------

function lathe.speed(v)
	local old = nc_state.speed
	if old ~= v then
		nc_state.speed = v
		print(word_append2("G96", "S", v, "M", 3))
	end
	return old
end


function lathe.feedrate(f)
	local old = nc_state.feed
	if old ~= f then
		nc_state.feed = f
		print("F" .. tostring(f))
	end
	return old
end


function lathe.set_rapid(f)
	local old = nc_state.feed_rapid
	nc_state.feed_rapid = f
	return old
end



return lathe
