------------------------------------------------------------------------
-- Crane wheels X-grooving. Main chunk.
--
-- (c) 2019 Aleksandr Loshkin <1214160@gmail.com>
-- This code is licensed under MIT licence (see LICENCE.TXT for details)
------------------------------------------------------------------------

data = require "x_groove_init"
geom = require "geom"
mach = require "lathe"


local h = (data.wheel.D - data.groove.d)/2
local r = data.tool.d / 2

local p1, p2, p3, p4, p_safe
do
	local l = math.tan(math.rad(data.groove.a)) * h
	p2 = { x = data.groove.d, z = (data.groove.w - data.wheel.w) / 2 }
	p3 = { x = data.groove.d, z = -(data.groove.w + data.wheel.w) / 2 }
	p1 = { x = data.wheel.D, z = p2.z + l }
	p4 = { x = data.wheel.D, z = p3.z - l }
	p_safe = { x = data.wheel.D + 2*data.dist.safe, z = data.dist.safe + r }
end

mach.rapidXZ(p_safe.x, p_safe.z)
mach.set_rapid(data.modes.feed_rapid)


-- Compensating shift to the right
function compens_shift_right(x, z, l, ang)
	local x, z = geom.shift_point(x, z, l, ang)
	x, z = geom.tool_rad_comp(x, z, r, ang - 90)
	return x, z
end



------------------
-- Debug output --
------------------

if debug then
	mach.comment("----- Debug -----")
	local l, x, z

	-- Right flange and chamfer
	mach.rapidXZ(data.wheel.D, 0)
	l = geom.rnd_corner(data.groove.rnd, 90 + data.groove.a)
	mach.workZ(p1.z + l)
	x, z = geom.shift_point(p1.x, p1.z, l, 270 - data.groove.a)
	mach.radiusCCW(x, z, data.groove.rnd)
	
	-- Right wall and cornder radius, groove diameter
	l = geom.rnd_corner(data.groove.r, 90 + data.groove.a)
	x, z = geom.shift_point(p2.x, p2.z, l, 90 - data.groove.a)
	mach.workXZ(x, z)
	l = geom.rnd_corner(data.groove.r, 90 + data.groove.a)
	mach.radiusCW(p2.x, p2.z - l, data.groove.r)
	mach.workZ(p3.z + l)

	-- Cornder radius, left wall, left flange
	x, z = geom.shift_point(p3.x, p3.z, l, 90 + data.groove.a)
	mach.radiusCW(x, z, data.groove.r)
	mach.workXZ(p4.x, p4.z)
	mach.workZ(-data.wheel.w)
end



--------------
-- Roughing --
--------------

if data.stage == "rough" or data.stage == "all" then
	local x, z, ang     -- Right wall
	local x1, z1, ang1  -- Left wall
	
	mach.comment("---- Roughing -----")
	mach.speed(data.modes.speed)

	-- Approach
	local l = geom.rnd_corner(r, 90 + data.groove.a)
	ang = 270 - data.groove.a
	x, z = compens_shift_right(p1.x, p1.z - data.allowance.z, -(l+data.dist.plunge), ang)
	mach.rapidX(p_safe.x)
	mach.rapidZ(z)
	mach.rapidX(x)
	x, z = geom.shift_point(x, z, data.dist.plunge, ang)
	mach.feedrate(data.modes.feed)
	mach.workXZ(x, z)
	
	-- Cycle
	local h_rough = h - data.allowance.x
	local n = math.ceil(h_rough / data.modes.depth)
	local t = h_rough / n  -- TO-DO: t only in X direction
	
	-- Outgo preparing
	ang1 = 90 + data.groove.a
	x1, z1 = compens_shift_right(p4.x, p4.z + data.allowance.z, l, ang1)
	
	for i = 1, n do
		x, z = geom.shift_point(x, z, t, ang)
		mach.feedrate(data.modes.feed_cutin)
		mach.workXZ(x, z)
	
		x1, z1 = geom.shift_point(x1, z1, -t, ang1)
		mach.feedrate(data.modes.feed)
		mach.workZ(z1)
		
		mach.workX(x1 + data.dist.plunge)
		mach.rapidZ(z)
		mach.workX(x)
	end
	
	mach.rapidX(p_safe.x)
	mach.rapidZ(p_safe.z)
end



---------------
-- Finishing --
---------------

if data.stage == "finish" or data.stage == "all" then
	local l, x, z, ang
	
	mach.comment("----- Finishing -----")
	mach.feedrate(data.modes.feed_finish)
	
	-- p1: approach and rounding
	mach.rapidX(p_safe.x)
	l = geom.rnd_corner(data.groove.rnd, 90 + data.groove.a)
	mach.rapidZ(p1.z + l)
	mach.rapidX(p1.x + 2*data.dist.plunge)
	mach.workX(p1.x)
	ang = 270 - data.groove.a
	x, z = compens_shift_right(p1.x, p1.z, l, ang)
	mach.radiusCCW(x, z, data.groove.rnd + r)
	
	-- p2: left wall and cornder radius
	l = geom.rnd_corner(data.groove.r, 90 + data.groove.a)
	x, z = compens_shift_right(p2.x, p2.z, -l, ang)
	mach.workXZ(x, z)
	mach.radiusCW(p2.x, p2.z - l, data.groove.r - r)

	-- p3: groove diameter, cornder radius
	mach.workZ(p3.z + l)
	ang = 90 + data.groove.a
	x, z = compens_shift_right(p3.x, p3.z, l, ang)
	mach.radiusCW(x, z, data.groove.r - r)
	
	-- p4: wall, outgo
	x, z = compens_shift_right(p4.x, p4.z, 0, ang)
	mach.workXZ(x, z)
	mach.workX(x + 2*data.dist.plunge)
	mach.rapidX(p_safe.x)
	mach.rapidZ(p_safe.z)
end
