------------------------------------------------------------------------
-- Crane wheels X-grooving. Initial data description.
--
-- (c) 2019 Aleksandr Loshkin <1214160@gmail.com>
-- This code is licensed under MIT licence (see LICENCE.TXT for details)
------------------------------------------------------------------------


data = {}


-- Overall dimensions
data.wheel = {
	D = 790,
	w = 200,
}

data.groove = {
	d = 700,   --Diameter
	w = 90,    --Width
	a = 11.31, --Angle: atan[1/5]=11.31
	r = 8,     --Radii in the inner angle
	rnd = 0.5, --Chamfer
}

-- Allowance for finishing
data.allowance = {
	x = 0.5,   -- Radial
	z = 0.5,
}

data.tool = {
	d = 10
}

data.modes = {
	speed = 150,
	depth = 3,
	feed = 0.5,
	feed_cutin = 0.1,
	feed_finish = 0.4,
	feed_rapid = 10000 -- mm/min
}

data.dist = {
	safe = 10,
	plunge = 1
}

-- Stage: "rough", "finish" or "all"
data.stage = "all"


-- Enable debug output
debug = true


return data
