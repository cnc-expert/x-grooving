------------------------------------------------------------------------
-- Crane wheels X-grooving. Geometry library.
--
-- (c) 2019 Aleksandr Loshkin <1214160@gmail.com>
-- This code is licensed under MIT licence (see LICENCE.TXT for details)
------------------------------------------------------------------------


geom = {}


-- Leg of the arc 'r' in the corner 'ang'
function geom.rnd_corner(r, ang)
	return r / math.tan(math.rad(ang/2))
end


-- Shift point (x,z) at angle 'ang'
function geom.shift_point(x, z, dist, ang)
	local x_shf = x + 2*dist * math.sin(math.rad(ang))
	local z_shf = z + dist * math.cos(math.rad(ang))
	return x_shf, z_shf
end



function geom.tool_rad_comp(x, z, r, ang)
	local x_shf, z_shf = geom.shift_point(x, z, r, ang)
	return x_shf - 2*r, z_shf
end



-------------------------
-- Circle intersection --
-------------------------

-- Central angles of two intersecting circles 'r1' and 'r2'
-- with intercentre distance 'dist', in rad
local function central_angles(r1, r2, dist)
	local a1, a2
	
	-- Law of cosines: R^2 = r^2 + c^2 - 2rc*cos(a)
	a1 = math.acos((r1^2 + dist^2 - r2^2) / (2*r1*dist))
	a2 = math.acos((r2^2 + dist^2 - r1^2) / (2*r2*dist))
	return 2*a1, 2*a2
end


local function central_angle_with_saggita(r, sag)
	-- en.wikipedia.org/wiki/Circular_segment
	return 2 * math.acos(1 - sag/r)
end


-- Area of circular segment 'r' with central angle 'a' in rad
local function segment_area(r, a)
	-- en.wikipedia.org/wiki/Circular_segment
	return r^2/2 * (a - math.sin(a))
end


function circles_intersection_area(r1, r2, dist)
	local a1, a2 = central_angles(r1, r2, dist)
	local s1 = segment_area(r1, a1)
	local s2 = segment_area(r2, a2)
	return s1 + s2
end

--function area_


return geom
